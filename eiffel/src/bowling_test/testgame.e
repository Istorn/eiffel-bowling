note
	description: "Summary description for {TESTGAME}."
	author: "Lorenzo Neri - lorenzo.neri@studenti.unimi.it"
	date: "$Date$"
	revision: "$Revision$"

class
	GAME_TEST_SET

inherit
	EQA_TEST_SET

redefine
	on_prepare
end

feature
	g: GAME

feature
	on_prepare
		do
			create g.make
		end

feature
	testGutterGame
		require
			g.score=0
		do
			rollMany(20,0)
		ensure
			g.score=0
		end

feature
	testAllOnes
		require
			g.score=0
		do
			rollMany(20,1)
		ensure
			g.score=20
		end


feature
	testOneSpare
		require
			g.score=0
		do
			rollSpare
			g.roll(3)

			rollMany(17,0)
		ensure
			g.score=16
		end

feature
	testOneStrike
		require
			g.score=0
		do
			rollStrike
			g.roll(3)
			g.roll(4)
			rollMany(16,0)
		ensure
			g.score=24

		end

feature
	testPerfectGame
		require
			g.score=0
		do
			rollMany(12,10)
		ensure
			g.score=300
		end

feature
	testLastSpare
		require
			g.score=0
		do
			rollMany(9,10)
			rollSpare
			g.roll(10)
		ensure
			g.score=279

		end

feature
	rollSpare
		do
			rollMany(2,5)
		ensure
			g.score> old g.score+10
		end

feature
	rollStrike
		do
			g.roll(10)
		ensure
			g.score> old g.score+10
		end

feature
		rollMany(n: INTEGER; pins: INTEGER)
		require
			(n>0 and pins>=0) and (n/=Void and pins/=Void)
		local
			i: INTEGER

		do
			from
				i:=0
			until
				i=n-1
			loop
				g.roll(pins)
				i:=i+1
			end
		ensure

			g.score >= old g.score + ((n-1)*pins)
		end

end
