note
	description: "Summary description for {GAME}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	GAME
create
	make
feature
	rolls:ARRAY[INTEGER]


feature
	currentRoll:INTEGER

feature
	make
		do
			create rolls.make_filled (0, 0, 20)
			create currentRoll
			currentRoll:=0
			ensure
					is_clean
		end

feature
	roll(pins: INTEGER)
		do
			currentRoll:=currentRoll+1
			rolls[currentRoll]:=pins
			ensure
				currentRoll=old currentRoll+1
		end
	is_clean:BOOLEAN
		local
			i:INTEGER
		do
			from
				i:=0
			until
				i=20
			loop
				if rolls[i]/=0 then
					Result:=false
				end
			end
			Result:=true
		end
feature
	score:INTEGER
	local
		iscore: INTEGER
		frameIndex:INTEGER
		frame: INTEGER
	do
		from
			iscore:=0
			frameIndex:=0
			frame:=0
		until
			frame=9
		loop
			if (isStrike(frameIndex)=true) then
						iscore:=iscore+10+strikeBonus(frameIndex)
						frameIndex:=frameIndex+1

					elseif (isSpare(frameIndex)=true) then
						iscore:=iscore+10+spareBonus(frameIndex)
						frameIndex:=frameIndex+2

					else
						iscore:=score+sumOfBallsInFrame(frameIndex)
						frameIndex:=frameIndex+2
					end
		end
		Result:=iscore
	end
feature
	isStrike(frameIndex:INTEGER):BOOLEAN
	do
		Result:=(rolls[frameIndex]=10)
	end

feature
	sumOfBallsInFrame(frameIndex: INTEGER):INTEGER
	do
		Result:=(rolls[frameIndex]+rolls[frameIndex+1])
	end
feature
	spareBonus(frameIndex:INTEGER):INTEGER
	do
		Result:=rolls[frameIndex+2]
	end

feature
	strikeBonus(frameIndex:INTEGER):INTEGER
	do
		Result:=rolls[frameIndex+1]+rolls[frameIndex+2]
	end

feature
	isSpare(frameIndex:INTEGER):BOOLEAN
	do
		if (rolls[frameIndex]+rolls[frameIndex+1])=10 then
			Result:=true
		else
			Result:=false
		end

	end
end
